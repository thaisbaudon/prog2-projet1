#pragma once

#include <vector>
#include <string>
#include "cell.hh"
#include "object.hh"

/** \file*/

typedef std::vector<Cell*>::iterator obj_iter;
typedef std::vector<bool>::iterator mark_iter;

/**
 * Singleton class in which we only use the static functions and attributes.
 They allow us to manipulate any objects created so as to manage the memory
 with a garbage collector.
 Functions allow us to mark an object or to create new ones.
 */
class GarbageCollector
{
public:
    static void mark(Object obj);
    static void unmark_all();
    static bool is_marked(Object obj);
    static void sweep(); // Delete all unmarked objects
    static void delete_everything();
    
    static Cell_Number* new_number(int contents);
    static Cell_String* new_string(std::string contents);
    static Cell_Symbol* new_symbol(std::string contents);
    static Cell_Pair* new_pair(Cell* car, Cell* cdr);
    static Cell_Subr* new_subr(std::string name);

    static bool memtrace;

private:
    static std::vector<Cell*> obj_register;
    static std::vector<bool> mark_register;
    static int find_obj_id(Object obj);
    static unsigned iter_to_id(obj_iter it);
    static void insert_new_object(Cell* new_obj);

    inline GarbageCollector();
    inline ~GarbageCollector();
};
