#include <iostream>
#include <exception>
#include <cstring>
#include "error.hh"
#include "object.hh"
#include "read.hh"
#include "eval.hh"
#include "mark_and_sweep.hh"

using namespace std;

int main(int argc, char** argv)
{
    // Enable debug or memory trace
    if (argc > 1 && strcmp(argv[1], "--debug") == 0)
	set_debug_status((argc > 1 && strcmp(argv[1], "--debug") == 0));

    if (argc > 1 && strcmp(argv[1], "--memory") == 0)
	GarbageCollector::memtrace = true;
	    
    register_subroutines();
    
    bool cont = true;
    while(cont)
    {
	cout << "Lisp? " << flush;

	try
	{
	    Object l = read_object();
	    handle_read_object(l, cout);
	    
	    // Mark and sweep need to run in a stable state
	    mark_and_sweep(global_env);
	}

	catch (LispError& e)
	{
	    // Do not quit the interpreter for a mere LispError
	    cerr << e.what() << endl;
	}

	// More serious than just a LispError: quit the interpreter
	catch (exception& e)
	{
	    cerr << e.what() << endl;
	    clog << "Exit" << endl;
	    cont = false;
	}
    }

    // Clean up the memory
    GarbageCollector::delete_everything();

    return 0;
}
