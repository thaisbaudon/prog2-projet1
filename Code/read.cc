#include <iostream>
#include <stdexcept>

#include "object.hh"
#include "read.hh"
#include "env.hh"
#include "subr.hh"
#include "eval.hh"

using namespace std;

extern Object get_read_Object();
extern int yyparse();
extern void yyrestart(FILE *fh);

static bool handle_defun_macro(Object obj, ostream& ostr)
{
    if (listp(obj) && eq(car(obj),symbol_to_Object("defun"))) // (defun ...)
    {
	/* (defun f (args) (body)) is expanded to
	   (setq f (lambda (args) (body))) */
	Object f = cadr(obj);
	assert(symbolp(f) || subrp(f));

	Object function_body = cddr(obj);
	assert(listp(function_body));

	Object lambda_expr = cons(symbol_to_Object("lambda"), function_body);

	handle_read_object(cons(symbol_to_Object("setq"),
				cons(f, cons(lambda_expr, nil()))), ostr);
	return true;
    }

    else
	return false;
}

static bool handle_setq_directive(Object obj, ostream& ostr)
{
    if ( listp(obj) && eq(car(obj), symbol_to_Object("setq")) ) // (setq ...)
    {
	Object sym = cadr(obj);
	assert(symbolp(sym));
	    
	Object val = caddr(obj);

	/* Prevent unwanted quote-like behaviour
	   e.g. when typing (setq b (f a)), we want b to have f(a)
	   as a value, not the list (f a) */
	val = eval_trace(val, global_env);

	global_env = Environment(sym, val, global_env);
	ostr << "SET: " << sym << " = " << val << endl;

	return true;
    }

    return false;
}

/* Handle directives, expand macros or eval the read object,
   and print the appropriate output to the ostream */
void handle_read_object(Object obj, ostream& ostr)
{
    if (!handle_defun_macro(obj, ostr))
	if (!handle_setq_directive(obj, ostr))
	    // Eval (no directive found)
	    ostr << eval_trace(obj, global_env) << endl;
}

Object read_object() {
  if (yyparse() != 0)
    throw runtime_error("End of input stream");
  Object l = get_read_Object();

  return l;
}

void read_object_restart(FILE* fh) {
    yyrestart(fh);
}
