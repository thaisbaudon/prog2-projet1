#include <string>
#include <cassert>
#include <iostream>
#include "error.hh"
#include "object.hh"
#include "env.hh"

using namespace std;

<<<<<<< HEAD
Environment::Environment(const Object& sym, const Object& val,
			 const Environment& env)
{
=======
Environment Environment::global_environment(nil());

Environment::Environment(const Object& sym, const Object& val, const Environment& env){
>>>>>>> parent of 454d4ae... Plus aucun definitely lost dans valgrind, il reste plein de still reachable
    assert(symbolp(sym) && (listp(env.A_list) || null(env.A_list)));
    
    /* The resulting list is printed as (val, nil).
       Using cons(sym, val) makes printenv crash. */
    Object a = cons(sym, cons(val, nil()));
    
    A_list=cons(a, env.A_list);
}

static Object aux(const Object& l, const Object& sym)
{
    assert((listp(l) || null(l)) && symbolp(sym));
    
    if (null(l)) // Empty list -> not found
	throw LispError("Undefined variable: " + Object_to_string(sym));

    else
    {
	// a must be a couple: a = (sym, val), where sym is a symbol
	Object a = car(l);
	Object b = cdr(l);
	assert(listp(a));
	    
	Object sym2 = car(a);
	
	// cdr(a) is not what we want here ((val) instead of val)
	Object val = cadr(a);
	assert(symbolp(sym2));

	if (eq(sym, sym2))
	    return val;
	else
	    return aux(b,sym);
    }
}

Object Environment::find_val(const Object& sym) const
{
    assert(symbolp(sym));
    return aux(A_list, sym);
}



