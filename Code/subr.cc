#include "object.hh"
#include "error.hh"
#include "subr.hh"

Object read_object();

std::map<std::string, Func> Subroutine::subr_collection =
    std::map<std::string, Func>();

bool Subroutine::is_registered(const std::string& name)
{
    // Returns 1 if found, 0 if not
    return Subroutine::subr_collection.count(name); 
}
    
void Subroutine::register_subr(const std::string& name, const Func& func_arg)
{
    Subroutine::subr_collection.emplace(name, func_arg);
}

Object Subroutine::apply(const std::string& name, const Object& args)
{
    // Throws an out_of_range exception if the subr does not exist
    const Func& f = Subroutine::subr_collection.at(name);
    return f(args);
}

/* subroutines are limited to a certain number of arguments,
   they ignore the surplus entries.*/

Object plus_subr(const Object a)
{   if (not (listp(a)))
    {
        throw LispError("not enough minerals");
    }
    if (not (numberp(car(a)) and numberp(cadr(a))))
    {
        throw LispError("must add two numbers");
    }
    else
    {
        return number_to_Object( Object_to_number(car(a))
				 + Object_to_number(cadr(a)));
    }
}

Object minus_subr(const Object a)
 {   
     if (not (listp(a)))
     {
         throw LispError("not enough minerals");
     }
     if (not (numberp(car(a)) and numberp(cadr(a))))
     {   
         throw LispError("must substract two numbers");
     }       
     else
     {   
         return number_to_Object( Object_to_number(car(a))
				  - Object_to_number(cadr(a)));
     }       
 }

Object times_subr(const Object a)
 {   
     if (not (listp(a)))
     {
         throw LispError("not enough minerals");
     }
     if (not (numberp(car(a)) and numberp(cadr(a))))
     {   
         throw LispError("must multiply two numbers");
     }       
     else
     {   
         return number_to_Object( Object_to_number(car(a))
				  * Object_to_number(cadr(a)));
     }       
 }

Object equal_subr(const Object a)
{
    if (not (listp(a)))
	throw LispError("not enough minerals");
    else
	return bool_to_Object(eq(car(a), cadr(a)));
}

Object or_subr(const Object a)
{
    if (! (listp(a)))
	throw LispError("not enough minerals");
    else
        if (eq(car(a),t()) || eq(cadr(a),t()))
	    return t();
        else
	    return nil();
}

Object and_subr(const Object a)
{
     if (! (listp(a)))
	 throw LispError("not enough minerals");
     else
         if (eq(car(a), t()) && eq(cadr(a),t()))
	     return t();
         else
	     return nil();
}

Object read_subr(const Object a)
{
    return read_object();
}

void register_subroutines()
{
    Subroutine::register_subr("+", plus_subr);
    Subroutine::register_subr("-", minus_subr);
    Subroutine::register_subr("*", times_subr);
    Subroutine::register_subr("=", equal_subr);
    Subroutine::register_subr("or", or_subr);
    Subroutine::register_subr("and", and_subr);
    Subroutine::register_subr("read", read_subr);
}
