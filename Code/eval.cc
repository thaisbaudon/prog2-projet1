#include "eval.hh"

using namespace std;

// Debug mode ("trace") setter and getter
static bool debug_status;
bool get_debug_status()
{
    return debug_status;
}

void set_debug_status(bool stat)
{
    debug_status = stat;
}

/* Auxiliary function: if list = (obj1, ..., objn), return (v1, ..., vn), where
   vi = eval(obji) */
Object eval_each(const Object& list, const Environment& env)
{
    assert(null(list) || listp(list));
    if (null(list))
	return nil();
    
    else
    {
	Object val = eval_trace(car(list), env);
	return cons(val, eval_each(cdr(list), env));
    }
}

// f = if -> eval o1, if true then eval o2, else eval o3
static Object eval_if(const Object& obj, const Environment& env)
{
    Object condition = cadr(obj);
    Object if_true = caddr(obj);
    Object if_false = cadddr(obj);

    if (! null(eval_trace(condition, env)))
	return eval_trace(if_true, env);
    else
	return eval_trace(if_false, env);
}

/* f = cond -> if n = 0, return nil, else if the first clause is satisfied
   return its value,
   else evaluate the next clauses */
static Object eval_cond(const Object& obj, const Environment& env)
{
    if (null(cdr(obj))) // No clause
	return nil();

    else
    {
	Object first_clause = cadr(obj);
	Object next_clauses = cddr(obj);

	// The first clause is satisfied
	if (! null(eval_trace(car(first_clause), env)))
	    return eval_trace(cadr(first_clause), env);

	else
	    // Evaluate the remaining clauses
	    return eval_trace(cons(symbol_to_Object("cond"),next_clauses), env);
    }
}

// f = progn -> successively evaluate o1, ..., on, and return the last eval (on)
static Object eval_progn(const Object& obj, const Environment& env)
{
    if (null(cdr(obj)))
	return nil();

    else
    {
	Object first_clause = cadr(obj);
	Object next_clauses = cddr(obj);

	Object first_eval = eval_trace(first_clause, env);
	    
	if (null(next_clauses))
	    return first_eval;
	else
	    return eval_trace(cons(symbol_to_Object("progn"), next_clauses),
			      env);
    }
}

Object eval(const Object& obj, const Environment& env)
{
    // First assume obj is an atom (nil, t, number, string or symbol)
    if (null(obj) || eq(obj, t()) || numberp(obj) || stringp(obj) || subrp(obj))
	return obj;

    // If obj is a symbol, look up this symbol in env and evaluate the result
    if (symbolp(obj))
	return env.find_val(obj);

    // At this point, obj is a list: obj = (f o1 ... on)
    Object f = car(obj);
    
    // Is f a symbol associated with a control structure ?
    // f = quote -> return o1 and ignore the remaining objects
    if (eq(symbol_to_Object("quote"), f))
	return cadr(obj);

    else if (eq(symbol_to_Object("printenv"), f)) // printenv
	return global_env.get_contents();
    
    else if (eq(symbol_to_Object("if"), f))
	return eval_if(obj, env);

    else if (eq(symbol_to_Object("cond"), f))
	return eval_cond(obj, env);

    else if (eq(symbol_to_Object("progn"), f))
	return eval_progn(obj, env);

    // A lambda-expression's value is itself
    else if (eq(symbol_to_Object("lambda"), f))
	return obj;
    
    /* At this point, f is a user-defined function or a subr:
       first evaluate the parameters in the current env, 
       then apply f to these values in env */
    else
    {
	Object lvals = eval_each(cdr(obj), env);
	return apply(f, lvals, env);
    }
}


// Wrap the eval function in eval_trace to print trace
Object eval_trace(const Object& obj, const Environment& env)
{
    static unsigned level = 0;
    if (get_debug_status())
    {
	clog << level << " --> " << obj << "\t| " << env.get_contents() << endl;
	level++;
	Object result = eval(obj, env);
	level--;
	clog << level << " <-- " << result << endl;
	
	return result;
    }

    else
	return eval(obj, env);
}
