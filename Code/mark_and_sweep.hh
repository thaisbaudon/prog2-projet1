#pragma once

#include "object.hh"
#include "env.hh"
#include "garbage_collector.hh"

/** \file */

/**
 * Marks all cells on which obj depends using a DFS
 */
void mark_dfs(const Object& obj);

/**
 * Applies mark_dfs on all objects from an environemnt
 */
void mark_env(const Object& env);

/**
 * Applies the DFS to all objects from an environment and then deletes
 all non marked objects from the register
 */
void mark_and_sweep(const Environment& env);
