#include "garbage_collector.hh"

using namespace std;

std::vector<Cell*> GarbageCollector::obj_register = vector<Cell*>();
std::vector<bool> GarbageCollector::mark_register = vector<bool>();

bool GarbageCollector::memtrace = false;

unsigned GarbageCollector::iter_to_id(obj_iter it)
{
    int id = it - obj_register.begin();
    assert(id >= 0);
    return (unsigned)id;
}

int GarbageCollector::find_obj_id(Object obj) // Return -1 if not found
{
    int id = -1;
    for (unsigned i = 0; i < obj_register.size(); i++)
    {
	if (obj_register.at(i) == obj)
	    id = i;
    }

    return id;
}

void GarbageCollector::mark(Object obj)
{
    int obj_id = find_obj_id(obj);
    if (obj_id >= 0) /*Ignore the cases where obj is not found (e.g. trying
		       to mark nil)*/
    {
	mark_register.at(obj_id) = true;

	if (memtrace)
	    clog << "Marked " << obj << " @ " << (void*)obj << endl;

	// If obj is a list, we need to mark its components too*
	if (listp(obj))
	{
	    mark(car(obj));
	    mark(cdr(obj));
	}
    }
}

void GarbageCollector::unmark_all()
{
    if (memtrace)
	clog << "Unmarking all " << obj_register.size() << " cells" << endl;
    
    for (mark_iter it = mark_register.begin(); it != mark_register.end(); it++)
	*it = false;
}

bool GarbageCollector::is_marked(Object obj)
{
    unsigned obj_id = find_obj_id(obj);
    assert(obj_id >= 0);
    return mark_register.at(obj_id);
}

void GarbageCollector::delete_everything() // Final clean-up
{
    unmark_all();
    sweep();
}	

void GarbageCollector::sweep() // Delete all unmarked objects
{
    /* We need to store elements that are to be deleted in a
       temporary container in order to keep indexes consistent
       within the loop */

    vector<unsigned> delete_these;
    
    for (unsigned i = 0; i < obj_register.size(); i++)
    {
	if (! mark_register.at(i))
	    delete_these.push_back(i);
    }

    // Delete 'em all
    if (! delete_these.empty())
    {
	for (int i = delete_these.size() - 1; i >= 0; i--)
	{
	    Object obj = obj_register.at(delete_these.at(i));
	    if (memtrace)
		clog << "Deleting " << obj << " @ " << (void*)obj << endl;

	    delete obj_register.at(delete_these.at(i));
	    obj_register.erase(obj_register.begin() + delete_these.at(i));
	    mark_register.erase(mark_register.begin() + delete_these.at(i));
	}
    }

    if (memtrace)
	clog << "Sweep complete; " << obj_register.size()
	     << " cell(s) remaining" << endl;
}

void GarbageCollector::insert_new_object(Cell* new_obj)
{
    assert(obj_register.size() == mark_register.size()); // Ensure that the new object's id is consistent within the two vectors
    obj_register.push_back(new_obj);
    
    // There is no need to mark new objects
    mark_register.push_back(false);
}

Cell_Number* GarbageCollector::new_number(int contents)
{
    Cell_Number* new_obj = new Cell_Number(contents);
    insert_new_object(new_obj);
    
    if (memtrace)
	clog << "New Cell_Number " << new_obj
	     << " @ " << (void*)new_obj << endl;

    return new_obj;
}

Cell_String* GarbageCollector::new_string(string contents)
{
    Cell_String* new_obj = new Cell_String(contents);
    insert_new_object(new_obj);

    if (memtrace)
	clog << "New Cell_String " << new_obj
	     << " @ " << (void*)new_obj << endl;
    
    return new_obj;
}

Cell_Symbol* GarbageCollector::new_symbol(std::string contents)
{
    Cell_Symbol* new_obj = new Cell_Symbol(contents);
    insert_new_object(new_obj);

    if (memtrace)
	clog << "New Cell_Symbol " << new_obj
	     << " @ " << (void*)new_obj << endl;
    
    return new_obj;
}

Cell_Pair* GarbageCollector::new_pair(Cell* car, Cell* cdr)
{
    Cell_Pair* new_obj = new Cell_Pair(car, cdr);
    insert_new_object(new_obj);

    if (memtrace)
	clog << "New Cell_Pair " << new_obj << " @ " << (void*)new_obj << endl;

    return new_obj;
}

Cell_Subr* GarbageCollector::new_subr(std::string name)
{
    Cell_Subr* new_obj = new Cell_Subr(name);
    insert_new_object(new_obj);

    if (memtrace)
	clog << "New Cell_Subr " << new_obj << " @ " << (void*)new_obj << endl;
    
    return new_obj;
}
