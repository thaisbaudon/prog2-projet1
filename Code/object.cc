#include <string>
#include <iostream>
#include <cassert>

#include "garbage_collector.hh"
#include "subr.hh"
#include "object.hh"
#include "cell.hh"

using namespace std;

<<<<<<< HEAD
Object nil()
{
    /* nil_obj can safely be set to static, since M&S will
       always mark nil (it won't be deleted; the returned
       address will remain the same)
     */
    static Cell* nil_obj = GarbageCollector::new_symbol("nil");
    return nil_obj;
=======
Object nil() {
    // static Cell *c = new Cell_Symbol("nil");
    static Cell* c = GarbageCollector::new_symbol("nil"); // The adress will stay the same
    return c;
>>>>>>> parent of 454d4ae... Plus aucun definitely lost dans valgrind, il reste plein de still reachable
}

bool null(const Object& l)
{
  return l == nil();
}

<<<<<<< HEAD
Object t()
{
    static Cell* c = GarbageCollector::new_symbol("t");
=======
Object t() {
    // static Cell *c = new Cell_Symbol("t");
    Cell* c = GarbageCollector::new_symbol("t");
>>>>>>> parent of 454d4ae... Plus aucun definitely lost dans valgrind, il reste plein de still reachable
    return c;
}

Object cons(const Object& a, const Object& l)
{
    return GarbageCollector::new_pair(a, l);
}

Object car(const Object& l) {
  assert(listp(l));
  return (dynamic_cast<Cell_Pair*>(l))->get_car();
}

Object cdr(const Object& l) {
  assert(listp(l));
  return (dynamic_cast<Cell_Pair*>(l))->get_cdr();
}

Object number_to_Object(int n) {
    return GarbageCollector::new_number(n);
}

Object string_to_Object(string s) {
    return GarbageCollector::new_string(s);
}

Object symbol_to_Object(string s)
{
    if (Subroutine::is_registered(s))
	return GarbageCollector::new_subr(s);

    else
	return GarbageCollector::new_symbol(s);
}

Object bool_to_Object(bool b) {
  if (b) return t();
  return nil();
}

int Object_to_number(const Object& l) {
  assert(numberp(l));
  return (dynamic_cast<Cell_Number*>(l))->get_contents();
}

string Object_to_string(const Object& l) {
    assert(stringp(l) || symbolp(l) || subrp(l));
    if (stringp(l))
	return (dynamic_cast<Cell_String*>(l))->get_contents();
    if (symbolp(l))
	return (dynamic_cast<Cell_Symbol*>(l))->get_contents();
    if (subrp(l))
	return (dynamic_cast<Cell_Subr*>(l))->get_subr();
    assert(false);
}

Object apply_subr(const Object& s, const Object& l)
{
    assert(s != nullptr && l != nullptr && s->get_sort() == Cell::sort::SUBR);
    Cell_Subr* cast_s = dynamic_cast<Cell_Subr*>(s);
    assert(Subroutine::is_registered(cast_s->get_subr()));
    return Subroutine::apply(cast_s->get_subr(), l);
}

bool numberp(const Object& l) {
  assert(l != nullptr);
  return (l->get_sort() == Cell::sort::NUMBER);
}

bool stringp(const Object& l) {
  assert(l != nullptr);
  return (l->get_sort() == Cell::sort::STRING);
}

bool symbolp(const Object& l) {
  assert(l != nullptr);
  return (l->get_sort() == Cell::sort::SYMBOL);
}

bool listp(const Object& l) {
  assert(l != nullptr);
  return (l->get_sort() == Cell::sort::PAIR);
}

bool subrp(const Object& l)
{
    assert(l != nullptr);
    return (l->get_sort() == Cell::sort::SUBR);
}

static void print_aux(ostream& s, const Object& l);

ostream& operator << (ostream& s, const Object& l) {
  if (null(l)) return s << "()";
  if (numberp(l)) return s << Object_to_number(l);
  if (stringp(l)) return s << Object_to_string(l);
  if (symbolp(l)) return s << Object_to_string(l);
  if (subrp(l)) return s << Object_to_string(l);
  assert(listp(l));
  s << "(";
  print_aux(s, l);
  s << ")";
  return s;
}

static void print_aux(ostream& s, const Object& l) {
  assert(listp(l));
  if (null(l)) return;
  if (null(cdr(l))) {
    s << car(l);
    return;
  }
  s << car(l) << " ";
  print_aux(s, cdr(l));
}

Object cadr(const Object& l) {
  return car(cdr(l));
}
Object cddr(const Object& l) {
  return cdr(cdr(l));
}
Object caddr(const Object& l) {
  return car(cddr(l));
}
Object cdddr(const Object& l) {
  return cdr(cddr(l));
}
Object cadddr(const Object& l) {
  return car(cdddr(l));
}

bool eq(const Object& a, const Object& b) {
  assert(a != nullptr);
  assert(b != nullptr);
  if (a->get_sort() != b->get_sort()) return false;
  if (numberp(a)) return Object_to_number(a) == Object_to_number(b);
  if (stringp(a) || symbolp(a))
      return Object_to_string(a) == Object_to_string(b);
  return a == b;
}
