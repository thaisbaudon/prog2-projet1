#include "error.hh"
#include "object.hh"
#include "env.hh"
#include "eval.hh"

Environment extend_env (const Object& sym, const Object& val,
			const Environment& env)
{
     return Environment(sym, val, env);
}

Environment extend_largs_env (const Object& lpars, const Object& lvals,
			      const Environment& env)
{
    if (null(lpars))
    {
        if (null(lvals))
            return env;
        else
            throw LispError("Too many args");
    }
    
    else
    {
        if (null(lvals))
            throw LispError("Too many pars");
        else
            return extend_env(car(lpars), car(lvals),
			      extend_largs_env (cdr(lpars), cdr(lvals), env));
    }
}

Object apply (const Object& obj, const Object& lvals, const Environment& env)
{
    if ( null(obj) )
        throw LispError("cannot apply nil to lisp objects");

    if ( eq(obj, t()) )
        throw LispError("cannot apply t to lisp objects");

    if ( numberp(obj) )
        throw LispError("cannot apply number to lisp objects");

    if ( stringp(obj) )
        throw LispError("cannot apply string to lisp objects");

    if ( symbolp(obj) )
    {
        Object val = env.find_val(obj);
        return apply(val, lvals, env);
    }

    if (subrp(obj))
        return(apply_subr(obj, lvals));

    if ( listp(obj) )
    {
        if ( eq( car(obj), symbol_to_Object("lambda") ) )
        {
            Object body = caddr(obj);
            Object lpars = cadr(obj);
            Environment new_env = extend_largs_env(lpars, lvals, env);

            return eval(body, new_env);
        }
        else
        {
            throw LispError("not a lambda function");
        }
    }

    else
	throw LispError("Unknown object type");
}

