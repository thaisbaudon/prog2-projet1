#pragma once

#include <ostream>
#include "object.hh"

/** \file */

using namespace std;

Object read_object();
void read_object_restart(FILE* fh);
void handle_read_object(Object obj, ostream& ostr);
