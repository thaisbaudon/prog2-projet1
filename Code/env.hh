#pragma once

#include <string>
#include <cassert>
#include "object.hh"

/** \file */
#define global_env Environment::global_environment

class Environment {
private:
    Object A_list;
public:
    inline Environment(Object l) {
        assert(listp(l) || null(l));
        A_list = l;
    }
    
    /**
     * Creates an environment by adding an symbol and a value to an already
     existing environment
     * @return The environment composed of the preexisting one and the
     association added
     */
    Environment(const Object& sym, const Object& val, const Environment& env);
    
    /**
     * Finds the value associated with the entry symbol
     * @return the value associated with the symbol
     */
    Object find_val(const Object& sym) const;
    inline const Object get_contents() const
	{return A_list;}

    static Environment global_environment;
};
