#pragma once

#include <stdexcept>
#include <string>

using namespace std;

/** \file */

class LispError : public runtime_error
{
public:
    inline explicit LispError(const string& what_arg):
	runtime_error("Lisp error: " + what_arg) {}
    
    inline explicit LispError(const char* what_arg):
	runtime_error("Lisp error: " + string(what_arg)) {}
};
