#pragma once

/** \file */

#include "object.hh"
#include "env.hh"
#include "mark_and_sweep.hh"

/**
 * Evaluates an object in regard to the environment
 * @return The object resulting from the evaluation
 */
Object eval(const Object& obj, const Environment& env);

/**
 * Evaluates a list of objects
 * @return The list of evaluations
 */
Object eval_each(const Object& list, const Environment& env);

/**
 * Handle that calls eval and, if we are in debug mode, prints the logs
 * associated with the execution
 * @return The evaluation of the object in argument
 */
Object eval_trace(const Object& obj, const Environment& env);

/**
 * Called in eval when given a function, it applies the function (or subroutine)
 * to the list of arguments lvals, after evaluation of the elements in lvals
 * @return The result of the function applied to the lvals list
 */
Object apply (const Object& obj, const Object& lvals, const Environment& env);

// Debug mode
bool get_debug_status();
void set_debug_status(bool stat);

