\documentclass[a4paper,11pt]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Global structure parameters
\usepackage{fullpage}%

\usepackage[french]{babel}%

\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%

% Font selection: http://www.tug.dk/FontCatalogue/newpx/
\usepackage{newpxtext}%
\usepackage{newpxmath}%

% Macro packages
\usepackage{url}%
\usepackage{graphicx}%
\usepackage{listings}%

% Parameters for listings
\lstset{%
  basicstyle=\sffamily,%
  columns=fullflexible,%
  frame=lb,%
  frameround=fftf,%
}%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\title{Prog 2 - Projet 1 - Interprète Lisp}

\author{Thaïs Baudon \and Grégoire Pacreau \and Thibault Sève-Minnaert}

\date{27 février 2018}

\maketitle

\begin{abstract}
	Nous présentons un interprète Lisp codé en C++ et utilisant le parser Bison et le lexer Flex. Nous en détaillons l'implémentation et notamment celle du garbage collector.
\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

	Le Lisp (abréviation de List Processing) est un langage de programmation inventé par John McCarthy et dont la première version est sortie en 1958. Il s'agit du premier langage de programmation fonctionnel.
	Sa syntaxe est très simple, on peut donc assez facilement produire un interprète. Celui que nous avons programmé utilise les outils Bison et Flex pour lire l'entrée et l'analyser. Les objets obtenus sont ensuite traités par un programme écrit en C++.
        Flex et Bison permettent de générer respectivement un \emph{lexer} et un \emph{parser}, qui transforment le contenu de l'entrée standard en objets C++ de différents types. Ces deux programmes faisaient partie du code de base à partir duquel nous avons développé notre interprète. 
        Nous avons réalisé un interprète Lisp dynamique, auquel nous avons ajouté un \emph{garbage collector} qui détruit les objets Lisp inutilisés. Ce dernier ne règle cependant pas toutes les fuites mémoire.

\section{Structure de l'interprète}

	\subsection{Environnement Lisp}
	
		L'environnement Lisp est l'ensemble des liens entre symboles et objets créés par l'utilisateur. Nous représentons son contenu par une liste Lisp construite par ajout d'une liaison à un environnement existant. Le contenu de l'environnement global est donc initialisé à \lstinline[language=c++]{nil()}. Cet environnement est géré par une variable globale, que l'on modifie au fil des instructions.
		
		La directive \lstinline[language=lisp]{setq} permet d'ajouter une liaison à l'environnement. Il s'agit de la seule directive qui modifie l'environnement global. Dans le code de l'interprète, aucune autre fonction ne modifie celui-ci. La fonction \lstinline[language=c++]{apply}, par exemple, récupère l'environnement et le duplique pour pouvoir lier chaque argument de la fonction à un paramètre, sans le modifier. C'est d'ailleurs une cause de fuite mémoire, puisque les objets créés par \lstinline[language=c++]{apply} n'existent plus pour l'interprète mais sont toujours réservés dans la mémoire.
	
	\subsection{Récursion}
	
		La récursion est bien gérée par l'interprète. Notre macro \lstinline[language=lisp]{defun} commence par ajouter une liaison à l'environnement, on peut donc appeler la fonction dans sa propre expression sans que l'interprète ne s'en inquiète. La récursivité est donc possible sans avoir à créer un alias (\emph{place-holder}) au préalable.
		
	\subsection{\emph{Subroutines}}
	
		Les \emph{subroutines} sont des fonctions prédéfinies dans l'interprète. On souhaite leur donner un statut spécial pour éviter que l'utilisateur ne les efface ou ne modifie leur sens, ce qui poserait problème puisque cela nous priverait de certaines fonctionnalités, notamment des tests logiques.
		
		Le type d'objet Lisp \lstinline[language=c++]{Cell_Subr} représente les \emph{subroutines}, identifiées par leur nom (sous la forme d'une chaîne de caractères). Les \emph{subroutines} sont enregistrées dans une collection associant des chaînes de caractères à des fonctions C++.
                Lorsque le parser identifie un objet comme un "symbole", la fonction \lstinline[language=c++]{symbol_to_Object} examine la collection afin de déterminer le véritable type de l'objet: si la chaîne de caractères lue se trouve dans la collection, l'objet est une subroutine et non un symbole.

                Le fonctionnement interne de la classe \lstinline[language=c++]{Subroutine}, qui gère la collection de \emph{subroutines}, est isolé du reste du code. Il s'agit d'une classe \emph{"singleton"}, sans instance, dont seules les méthodes statiques sont utilisées. Les fonctions C++ associées aux \emph{subroutines} sont de type \lstinline[language=c++]{std::function<Object(Object)>} (abrégé en \lstinline[language=c++]{Func}). La collection est implémentée sous la forme d'un conteneur associatif de type \lstinline[language=c++]{std::map<std::string, Func>}. Chaque \emph{subroutine} est accessible par son nom, ce qui permet d'éviter la manipulation d'objets fonctionnels dans le reste du code.

                
		De plus, il est impossible de masquer les liaisons entre les \emph{subroutines} et leurs symboles respectifs. En effet, la directive \lstinline[language=Lisp]{setq} impose d'associer un symbole à une valeur, et les symboles déjà associés à une \emph{subroutine} sont refusés (car ils sont interprétés comme des \emph{subroutines} et non des symboles).
		
		Le code de l'interprète a été conçu pour que l'ajout de nouvelles \emph{subroutines} soit aisé: il suffit de créer une fonction C++ prenant un \lstinline[language=c++]{Object} en paramètre et renvoyant un \lstinline[language=c++]{Object}, puis de l'ajouter à la collection, en lui associant un symbole.
		
		
	\subsection{Macro Expansion}
	
	Pour faciliter la manipulation de l'interprète, nous lui avons ajouté des macros qu'il reconnaît et remplace par leur définition.
        Cette macro expansion peut être réalisée au niveau du \emph{toplevel} ou directement au niveau du \emph{parser}.
		
		\subsubsection{Macro Expansion dans le \emph{toplevel}}
		
			Ici, la macro est considérée comme un symbole. On traite ce symbole dans le fichier \lstinline{read.cc} avec une fonction appelée en amont du traitement des directives. C'est cette fonction qui assure que la macro \lstinline[language=lisp]{defun} est interprétée comme un \lstinline[language=lisp]{setq} d'une $\lambda$-expression.
			
		\subsubsection{Macro Expansion dans le \emph{lexer}}
		
			Ici la macro n'est pas considérée comme un symbole, mais identifiée dès sa lecture par le \emph{lexer}. Par exemple, si le \emph{lexer} rencontre le caractère \lstinline[language=lisp]{'}, il renvoie un \emph{token} de type \lstinline[language=c++]{"Token_quote"}. C'est alors le \emph{parser} qui transforme ce \emph{token} en symbole \lstinline[language=c++]{"quote"}. Ainsi, \lstinline[language=lisp]{'(1 2)} est interprété \lstinline[language=lisp]{(quote (1 2))}.

                        \subsubsection{Comparaison des deux approches}
                        L'entrée \lstinline[language=lisp]{(quote defun)} donne en sortie \lstinline[language=lisp]{defun}, car la macro \lstinline[language=lisp]{defun} est gérée au niveau du \emph{toplevel}.
                        En revanche, l'entrée \lstinline[language=lisp]{(quote '(1 2))} donne la sortie \lstinline[language=lisp]{quote (1 2)}, et non \lstinline[language=lisp]{'(1 2)}, car le \emph{lexer} transforme le symbole \lstinline[language=lisp]{'} en \lstinline[language=lisp]{quote} avant l'interpétation de l'expression.
	
\section{Gestion des erreurs}

	\subsection{La classe \lstinline[language=c++]{LispError}}
	
	Pour éviter qu'une erreur de manipulation commise par l'utilisateur conduise systématiquement à l'arrêt de l'interprète, nous
        gérons les différentes erreurs avec des exceptions et des assertions. Pour gérer l'ensemble des erreurs liées à la manipulation de l'environnement Lisp, nous avons créé la classe \lstinline[language=c++]{LispError}. Il s'agit d'un type d'exception dérivé de la classe \lstinline[language=c++]{std::runtime_error}. Les \lstinline[language=c++]{LispError} sont rattrapées au niveau de la boucle principale de l'interprète. On affiche alors \lstinline[language=c++]{Lisp error:}, suivi du message renseigné dans le constructeur de \lstinline[language=c++]{LispError}. Cela permet en particulier de revenir à l'interprète une fois l'exception rattrapée; l'environnement présent avant l'appel à l'origine de l'erreur n'est pas perdu.
		
	\subsection{Le surplus d'arguments}
		
	Dans cet interprète, nous avons décidé de ne pas sanctionner le surplus d'arguments. Ce n'est pas le cas du défaut d'argument, qui lui provoque une erreur.
	
	Si une fonction se voit attribuer trop d'arguments, elle ne tient pas compte du surplus. Pour une fonction nécessitant $n$ arguments et en recevant $n+m$, les $m$ arguments surnuméraires sont ignorés. Ainsi \lstinline[language=lisp]{(+ 1)} déclenchera une erreur, mais \lstinline[language=lisp]{(+ 1 2 3)} renverra 3, c'est-à-dire que l'interprète évaluera \lstinline[language=lisp]{(+ 1 2)}.
			
	\subsection{La trace}
		
	À l'aide de la fonction \lstinline[language=c++]{printenv}, nous avons créé un mode de débogage. Ce mode affiche l'état de l'environnement à chaque étape de calcul. Il est accessible lors du lancement de l'interprète avec l'option \lstinline[language=bash]{--trace}.

\section{Gestion de la mémoire et \emph{Garbage Collector}}
	
	\subsection{La classe \lstinline[language=c++]{GarbageCollector}}
	
	La classe \lstinline[language=c++]{GarbageCollector} contient un tableau (\lstinline[language=c++]{std::vector}) de pointeurs \lstinline[language=c++]{Cell*}, ainsi qu'un tableau de booléens permettant de "marquer" certains objets.
	
	Comme \lstinline[language=c++]{Subroutine}, il s'agit d'une classe \emph{"singleton"}, dont seules les méthodes statiques sont accessibles.
	
	Elle fournit les méthodes \lstinline[language=c++]{new_number}, \lstinline[language=c++]{new_string}, \lstinline[language=c++]{new_symbol}, \lstinline[language=c++]{new_pair} et \lstinline[language=c++]{new_subr}, qui allouent une nouvelle cellule du type désiré et l'ajoutent à la collection d'objets. Tout nouvel objet doit être alloué en utilisant ces fonctions pour assurer la gestion de l'ensemble des cellules.
	
	\subsection{Algorithme \emph{Mark and Sweep}}
	
		Pour gérer les fuites de mémoire identifiées précédemment, nous mettons en place une stratégie du type \emph{Mark and Sweep} pour repérer les objets inutilisés.
		La classe \lstinline[language=c++]{GarbageCollector} fournit des méthodes pour "marquer" ou "démarquer" les objets.
		 Il suffit alors marquer tous les objets de l'environnement et ceux dont ils dépendent (c'est-à-dire les composants \lstinline[language=c++]{car} et \lstinline[language=c++]{cdr} des cellules de type \lstinline[language=c++]{Cell_Pair}), grâce à une descente en profondeur. On détruit ensuite tout objet du registre n'ayant pas de marque.
		
		L'algorithme est appelé à chaque fois que l'interprète a fini de traiter une commande de l'utilisateur. Plus précisément, à chaque appel de \lstinline[language=c++]{read} sur un objet lu, \lstinline[language=c++]{read} lance l'évaluation, puis lorsque l'exécution de \lstinline[language=c++]{read} est terminée, la fonction \lstinline[language=c++]{mark_and_sweep} nettoie la mémoire.
		
	\subsection{Une trace adaptée à la mémoire}
	
		Dans la mesure où nous tenons un registre de tous les objets créés par l'interprète, nous pouvons implémenter un mode de débogage qui suit les affectations et destructions de cellules. Pour y accéder, il suffit de lancer l'interprète avec l'option \lstinline[language=bash]{--memory}, comme pour l'autre mode que nous avons implémenté. Il affiche un message lors de la création ou de la destruction d'un objet Lisp, indiquant l'adresse de la cellule pointée.
		
	\subsection{Autres fuites}
	
		Lors de l'exécution de l'interprète avec l'outil Valgrind, nous avons repéré une fuite mémoire qui n'était pas gérée par le \emph{garbage collector}. Elle provenait de notre façon de gérer \lstinline[language=lisp]{nil()}. Pour éviter que la valeur de \lstinline[language=lisp]{nil()} ne change d'emplacement mémoire sans arrêt, nous la définissons comme une variable statique. Cela permet notamment au prédicat \lstinline[language=c++]{nullp} de fonctionner au niveau de la classe \lstinline[language=c++]{Object} en testant l'égalité des adresses. Cependant, le \emph{garbage collector} ne supprimait pas l'objet \lstinline[language=lisp]{nil}, même lors de l'arrêt de l'interprète.
		
		Pour résoudre ce problème, nous avons ajouté une fonction \lstinline[language=c++]{delete_everything}, appelée à la fin de la fontion \lstinline[language=c++]{main}. Elle libère les emplacements mémoires pris par l'environnement et \lstinline[language=c++]{nil()}, ce qui diminue les fuites mémoires. Cependant, il demeure des fuites que nous n'avons pas réussi à expliquer, en partie liées aux appels à \lstinline[language=c++]{strdup} effectués dans le \emph{parser}, mais aussi à des défauts dans notre \emph{garbage collector}. 

\section{Conclusion}

	Nous avons présenté une implémentation d'un interprète Lisp avec gestion des erreurs et \emph{garbage collector}. Cette implémentation peut être améliorée, notamment en gérant mieux la mémoire pour supprimer les dernières fuites. Nous n'avons pas non plus eu le temps d'ajouter la gestion des continuations dans l'interprète.
	
	Le projet a été l'occasion d'améliorer grandement nos capacités d'organisation lors du travail en groupe. Les premières phases du projet ont été très efficaces grâce à l'application de la méthode de développement \emph{agile}. Nous avons discuté au tableau de ce que nous voulions réaliser et avons pu prévoir notre code en conséquence. Par exemple, les décisions concernant la gestion du surplus d'arguments ou de l'environnement ont été prises très tôt dans le projet.
	
	Cependant, il nous reste des progrès à faire dans la correction des erreurs. Une erreur très simple à corriger dans le \emph{garbage collector} nous a grandement retardés le dernier jour. De plus, une mauvaise organisation pendant le dernier jour de développement nous a fait a fait dépasser l'heure limite de rendu du code, alors que le projet était fini à temps.

\end{document}
