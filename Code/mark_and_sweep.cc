#include <iostream>
#include "object.hh"
#include "env.hh"
#include "garbage_collector.hh"

using namespace std;

void mark_dfs(const Object& obj)
{
    if (! GarbageCollector::is_marked(obj))
    {
        if (numberp(obj) || stringp(obj) || symbolp(obj) || null(obj)
	    || eq(t(), obj))
            GarbageCollector::mark(obj);

        if (listp(obj))
        {
            mark_dfs(car(obj));
            mark_dfs(cdr(obj));
        }
    }
}

void mark_env(const Object& env)
{
    if (!null(env))
    {
	GarbageCollector::mark(car(env));
        mark_env(cdr(env));
    }
}

void mark_and_sweep(const Environment& env)
{
    if (GarbageCollector::memtrace)
	clog << "Beginning M&S" << endl;
    
    GarbageCollector::unmark_all();
    mark_env(env.get_contents());
    GarbageCollector::mark(env.get_contents());
    // Nil must keep the same address during the whole execution
    GarbageCollector::mark(nil());
    GarbageCollector::sweep();

    if (GarbageCollector::memtrace)
	clog << "M&S done" << endl;
}   
 
