#pragma once

#include <string>
#include <cassert>
#include "subr.hh"

/** \file */

/*****************************/

class Cell {
public:
    enum class sort {NUMBER, STRING, SYMBOL, PAIR, SUBR};
    virtual sort get_sort() const = 0;
    inline virtual ~Cell() {}
};

using Object = Cell*;

/*****************************/
class Cell_Number: public Cell {
private:
  int contents;
public:
    sort get_sort() const override;
    explicit Cell_Number(int _contents);
    int get_contents() const;
};

/*****************************/

class Cell_String: public Cell {
private:
  std::string contents;
public:
  sort get_sort() const override;
  explicit Cell_String(std::string s);
  std::string get_contents() const;
};

/*****************************/

class Cell_Symbol: public Cell {
private:
  std::string contents;
public:
  sort get_sort() const override;
  explicit Cell_Symbol(std::string s);
  std::string get_contents() const;
};

/*****************************/

class Cell_Pair: public Cell {
private:
  Cell *car;
  Cell *cdr;
public:
    sort get_sort() const override;
    explicit Cell_Pair(Cell *_car, Cell *_cdr);
    Cell *get_car() const;
    Cell *get_cdr() const;
};

// Subroutines
class Cell_Subr: public Cell
{
private:
    std::string subr;
public:
    inline sort get_sort() const override
	{return sort::SUBR;}

    inline explicit Cell_Subr(std::string subr_arg)
	{
	    assert(Subroutine::is_registered(subr_arg));
	    subr = subr_arg;
	}
	
    inline std::string get_subr() const
	{
	    return subr;
	}
};
