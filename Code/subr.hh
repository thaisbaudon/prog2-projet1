#pragma once

#include <string>
#include <map>
#include <functional>

/** \file*/
class Cell;
using Object = Cell*;
typedef std::function<Object(Object)> Func;

/**
 * This class allows us to apply and register the subroutines, 
 ensuring that they cannot be modified by the user.
 */
class Subroutine
{
private:
    inline Subroutine()	{}
    inline ~Subroutine() {}
    
public:
    static std::map<std::string, Func> subr_collection;
    
    /**
     * Verifies that the string is related to a subroutine
     */
    static bool is_registered(const std::string& name);    
    
    /**
     * Adds a subroutine to the register and gives it a name.
     */
    static void register_subr(const std::string& name, const Func& func_arg);
    
    /**
     * Applies the subroutine to an object.
     The subroutine is identified by the string attached to it
     * @return The result of the application of the subroutine
     */
    static Object apply(const std::string& name, const Object& args);
};

void register_subroutines();
